{
  "name": "swisnl/hedera",
  "description": "Website www.hedera.nl",
  "license": "proprietary",
  "authors": [
    {
      "name": "Swis BV",
      "email": "info@swis.nl"
    }
  ],
  "repositories": [
    {
      "type": "vcs",
      "url": "https://github.com/swisnl/viewfield.git"
    },
    {
      "type": "composer",
      "url": "https://packages.drupal.org/8"
    }
  ],
  "require": {
    "php": ">=7.4",
    "bugsnag/bugsnag": "^3.4",
    "composer/installers": "^2.0",
    "cweagans/composer-patches": "^1.7",
    "drupal-composer/drupal-l10n": "^2.0",
    "drupal/admin_toolbar": "^3.0",
    "drupal/admin_toolbar_entity_version": "^1.0",
    "drupal/admin_toolbar_messages": "^1.0",
    "drupal/admin_toolbar_tasks": "^1.0",
    "drupal/administerusersbyrole": "^3.0",
    "drupal/bem": "^3.0",
    "drupal/breadcrumb_menu": "^1.0",
    "drupal/config_split": "^1.4",
    "drupal/core": "9.4.*@stable",
    "drupal/ctools": "^3.11",
    "drupal/editor_advanced_link": "^2.0",
    "drupal/editor_file": "^1.0",
    "drupal/entity_reference_revisions": "^1.0",
    "drupal/entity_usage": "^2.0",
    "drupal/fieldblock": "^2.0",
    "drupal/focal_point": "^1.0",
    "drupal/google_tag": "^1.0",
    "drupal/image_widget_crop": "^2.3",
    "drupal/imageapi_optimize": "^4.0",
    "drupal/imageapi_optimize_binaries": "^1.0",
    "drupal/inline_entity_form": "^1.0",
    "drupal/linkit": "^6.0",
    "drupal/masquerade": "^2.0",
    "drupal/menu_admin_per_menu": "^1.0",
    "drupal/menu_trail_by_path": "^1.1",
    "drupal/metatag": "^1.0",
    "drupal/monolog": "^2.2",
    "drupal/override_node_options": "^2.0",
    "drupal/paragraphs": "^1.15",
    "drupal/paragraphs_edit": "^2.0",
    "drupal/pathauto": "^1.6",
    "drupal/redirect": "^1.0",
    "drupal/redis": "^1.4",
    "drupal/replicate": "^1.0",
    "drupal/replicate_ui": "^1.0",
    "drupal/reroute_email": "^2.0",
    "drupal/search_api": "^1.0",
    "drupal/simple_sitemap": "^4.0",
    "drupal/smart_trim": "^1.0",
    "drupal/snowball_stemmer": "^2.0",
    "drupal/swiftmailer": "^2.0",
    "drupal/taxonomy_access_fix": "^3.0",
    "drupal/twig_renderable": "^1.0",
    "drupal/view_unpublished": "^1.0",
    "drupal/viewfield": "8.3.*",
    "drupal/webform": "^6.1",
    "drush/drush": "^11.0",
    "liborm85/composer-vendor-cleaner": "^1.6",
    "vlucas/phpdotenv": "^5.4"
  },
  "require-dev": {
    "behat/behat": "^3.5",
    "drupal/coder": "^8.3.15",
    "drupal/config_update": "^1.0",
    "drupal/devel": "^5.0",
    "drupal/drupal-extension": "^4.1",
    "drupal/potx": "^1.0",
    "drupal/stage_file_proxy": "^1.1",
    "drupal/upgrade_status": "^3.0",
    "mglaman/phpstan-drupal": "^1.1",
    "mikey179/vfsstream": "^1.2",
    "phpmd/phpmd": "2.*",
    "phpspec/prophecy-phpunit": "^2.0",
    "phpstan/phpstan": "^1.5",
    "phpstan/phpstan-deprecation-rules": "^1.0",
    "phpunit/phpunit": "^9",
    "pyrech/composer-changelogs": "^1.4",
    "symfony/phpunit-bridge": "^6.0"
  },
  "conflict": {
    "drupal/app": "*",
    "drupal/backend": "*",
    "drupal/drupal": "*",
    "drupal/frontend": "*",
    "drupal/mailing": "*"
  },
  "minimum-stability": "dev",
  "prefer-stable": true,
  "config": {
    "preferred-install": "dist",
    "prepend-autoloader": true,
    "sort-packages": true,
    "allow-plugins": {
      "composer/installers": true,
      "cweagans/composer-patches": true,
      "dealerdirect/phpcodesniffer-composer-installer": true,
      "drupal-composer/drupal-l10n": true,
      "liborm85/composer-vendor-cleaner": true,
      "pyrech/composer-changelogs": true
    },
    "platform": {
      "php": "7.4.3"
    },
    "dev-files": {
      "no-dev-only": true
    }
  },
  "autoload": {
    "files": [
      "app/autoload.env.php"
    ]
  },
  "scripts": {
    "post-install-cmd": [
      "bash ./scripts/composer/post-install-cmd.sh"
    ],
    "check-style": "phpcs",
    "fix-style": "phpcbf"
  },
  "extra": {
    "installer-paths": {
      "app/core": [
        "type:drupal-core"
      ],
      "public_html/libraries/{$name}": [
        "type:drupal-library"
      ],
      "app/modules/contrib/{$name}": [
        "type:drupal-module"
      ],
      "app/profiles/contrib/{$name}": [
        "type:drupal-profile"
      ],
      "app/themes/contrib/{$name}": [
        "type:drupal-theme"
      ]
    },
    "drupal-l10n": {
      "destination": "../storage/translations",
      "languages": [
        "nl"
      ]
    },
    "composer-exit-on-patch-failure": true,
    "patchLevel": {
      "drupal/core": "-p2"
    },
    "patches-file": "composer.patches.json",
    "dev-files": {
      "*/*": [
        ".cspell.json",
        ".codeclimate.yml",
        ".ddev/",
        ".editorconfig",
        ".eslintignore",
        ".eslintrc",
        ".eslintrc.*",
        ".gitattributes",
        ".git/",
        ".github/",
        ".gitignore",
        ".php_cs",
        ".php_cs.dist",
        ".php-cs-fixer.dist.php",
        ".phpunit.result.cache",
        ".prettierignore",
        ".prettierrc.json",
        ".scrutinizer.yml",
        ".styleci.yml",
        ".stylelintignore",
        ".stylelintrc.json",
        ".travis.yml",
        ".travis-*.sh",
        ".tugboat/",
        "appveyor.yml",
        "test/",
        "tests/",
        "Tests/",
        "Dockerfile",
        "docs/",
        "grumphp.yml.dist",
        "Makefile",
        "package.json",
        "psalm.xml",
        "psalm.xml.dist",
        "psalm-baseline.xml",
        "phpcs.xml",
        "phpcs.xml.dist",
        "phpspec.yml",
        "phpstan.neon.dist",
        "phpunit.xml.dist",
        "postcss.config.js",
        "yarn.lock"
      ],
      "drupal/*": [
        "drupalci.yml"
      ],
      "drupal/core": [
        "/.env.example",
        "/assets/scaffold/",
        "/assets/vendor/ckeditor5/",
        "/modules/simpletest/",
        "/modules/ckeditor5/",
        "/profiles/*/",
        "!/profiles/minimal/",
        "/scripts/",
        "/themes/*/",
        "!/themes/claro/",
        "!/themes/classy/",
        "!/themes/engines/",
        "!/themes/seven/",
        "!/themes/stable/",
        "!/themes/stable9/"
      ]
    }
  }
}
