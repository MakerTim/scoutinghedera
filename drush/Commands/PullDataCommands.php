<?php

namespace Drush\Commands;

class PullDataCommands extends DrushCommands {

  /**
   * Pull data from the acceptance environment.
   *
   * @command pull:acc
   * @aliases pull-acc
   */
  public function pullAcc() {
    $this->pull('acc');
  }

  /**
   * Pull data from the production environment.
   *
   * @command pull:prod
   * @aliases pull-prod
   */
  public function pullProd() {
    $this->pull('prod');
  }

  protected function pull(string $environment) {
    $process = $this->processManager()->process(['bash', 'scripts/sh/pull.sh', $environment]);
    $process->setTty(true);
    $process->mustRun();
  }

}
