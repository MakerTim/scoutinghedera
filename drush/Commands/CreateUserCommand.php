<?php

namespace Drush\Commands;

final class CreateUserCommand extends DrushCommands {

  /**
   * Create a user with the administrator role and return a one-time-login.
   *
   * @command fluo:create-user
   * @aliases dw-cu
   */
  public function fluoCreateUser($mail = NULL, $options = ['role' => 'administrator', 'alias' => 'self']) {
    if (is_null($mail) || !$this->isValidEmail($mail)) {
      throw new \Exception(dt('A valid email has to be provided.'));
    }

    $process = $this->processManager()->process(['bash', 'scripts/sh/create-user.sh', $options['alias'], $mail, $options['role']]);
    $process->setTty(TRUE);
    $process->mustRun();

  }

  private function isValidEmail($email): bool {
    return filter_var($email, FILTER_VALIDATE_EMAIL) !== FALSE;
  }

}
