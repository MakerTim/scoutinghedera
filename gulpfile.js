/* global require */

const gulp = require('gulp');
const config = require('./gulpconfig');

function task(task) {
  return require('./scripts/gulp/' + task)(config);
}

gulp.task('browsersync', task('browsersync'));
gulp.task('watch', task('watch'));

gulp.task('build-favicons', task('build-favicons'));
gulp.task('build-fonts', task('build-fonts'));
gulp.task('build-images', task('build-images'));
gulp.task('build-js', task('build-js'));
gulp.task('build-libraries', task('build-libraries'));
gulp.task('build-scss', task('build-scss'));

gulp.task('build-phase1', gulp.parallel(
  'build-favicons',
  'build-fonts',
  'build-images',
  'build-libraries'
));

gulp.task('build-phase2', gulp.parallel(
  'build-scss',
  'build-js'
));

gulp.task('build', gulp.series('build-phase1', 'build-phase2'));

gulp.task('default', gulp.series('build-phase2'));

gulp.task('clean', task('clean'));

gulp.task('clean-build', gulp.series('clean', 'build'));

gulp.task('format-scss', task('format-scss'));

gulp.task('lint-js', task('lint-js'));
gulp.task('lint-scss', task('lint-scss'));

gulp.task('lint', gulp.parallel(
  'lint-js',
  'lint-scss'
));

gulp.task('lint-js-changes', task('lint-js-changes'));
gulp.task('lint-scss-changes', task('lint-scss-changes'));

gulp.task('lint-changes', gulp.parallel(
  'lint-js-changes',
  'lint-scss-changes'
));
