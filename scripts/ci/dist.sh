#!/usr/bin/env bash

set -euo pipefail

main() {
  local branch="${1:?'Missing argument 1 <branch>'}"
  local pull_request_branch="${2-}"

  check "$branch" "$pull_request_branch"

  build "$branch" "$pull_request_branch"
}

check() {
  local branch=$1
  local pull_request_branch="${2-}"

  echo "Checking $branch branch..."

  local remote_head; remote_head="$(git ls-remote origin "refs/heads/$branch" | cut -f 1)"

  # Fail if the branch does not exist remotely.
  if [ -z "$remote_head" ]; then
    echo "Branch origin/$branch does not exist." >&2
    return 1
  fi

  # For PRs skip comparing the last commits.
  if [ -n "$pull_request_branch" ]; then
    return 0
  fi

  local current_head; current_head="$(git rev-parse HEAD | cut -f 1)"
  local branch_head; branch_head="$(git rev-parse "$branch" | cut -f 1)"

  # Fail if not working on the latest commit.
  if [ "$current_head" != "$branch_head" ]; then
    echo "Current HEAD does not match head of $branch." >&2
    return 1
  fi

  # Fail if new commits have been added to the remote branch.
  if [ "$current_head" != "$remote_head" ]; then
    echo "Current HEAD does not match head of origin/$branch." >&2
    return 1
  fi

  return 0
}

build() {
  local branch=$1
  local pull_request_branch="${2-}"

  local target="$branch"
  if [ -n "$pull_request_branch" ]; then
    target="$branch--$pull_request_branch"
  fi

  echo "Building dist/$target branch..."

  git checkout -b "dist/$target"

  echo "Staging files from .distfiles..."
  # Add all files in .distfiles to be committed to the dist branch.
  # Skip empty lines and comment lines starting with a # in .distfiles.
  # Skip files listed in .distfiles that don't actually exist.
  # Use --force to also add files included in .gitignore.
  sed '/^\s*#/d' .distfiles | while read -r f; do test -e "$f" && echo "$f"; done | xargs git add --force

  echo "Preparing commit message..."
  # Build a commit message containing the prefix and the first line of the
  # last commit message. For PRs use the last commit of the PR branch.
  local msg; msg="DIST $(git log --pretty=%B -n 1 "${pull_request_branch:-"$branch"}" | head -n 1)"

  echo "Committing dist/$target branch..."
  # Use --allow-empty so the dist branch is build even when no files have changed.
  # Use --no-verify to bypass the pre-commit and commit-msg hooks.
  git commit --allow-empty --no-verify -m "$msg"

  echo "Pushing dist/$target branch..."
  # Use --force to replace to origin/dist branch.
  # Use --no-verify to bypass the pre-push hook.
  git push --force --no-verify origin "dist/$target"

  echo "Done"
}

main "$@"
