/* global module, require */

const gulp = require('gulp');
const stylelint = require('gulp-stylelint');

module.exports = function (config) {
  return function formatScss() {
    return gulp.src(config.path.ASSETS_SRC + '/**/*.scss')
      .pipe(stylelint({
        fix: true,
        syntax: 'scss'
      }))
      .pipe(gulp.dest(config.path.ASSETS_SRC));
  };
};
