const gulp = require('gulp');
const sass = require('gulp-dart-sass');
const globImporter = require('node-sass-glob-importer');
const sourcemaps = require('gulp-sourcemaps');

module.exports = function (config) {
  return function buildScss() {
    return gulp.src(config.path.ASSETS_SRC + '/themes/frontend/css/*.scss')
      .pipe(sourcemaps.init())
      .pipe(sass({
        outputStyle: 'compressed',
        precision: 10,
        importer: [
          globImporter()
        ],
      }).on('error', sass.logError))
      .pipe(sourcemaps.write('./'))
      .pipe(gulp.dest(config.path.ASSETS_DST + '/themes/frontend/css'))
  };
};
