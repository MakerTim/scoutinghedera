/* global module, require */

const gulp = require('gulp');
const newer = require('gulp-newer');
const imagemin = require('gulp-imagemin');
const merge = require('merge-stream');
const rename = require('gulp-rename');

module.exports = function (config) {
  return function buildImages() {
    let tasks = [];

    tasks.push(gulp.src([
      config.path.ASSETS_SRC + '/**/*.@(gif|jpg|png|svg|json)',
      // Exclude Images with .nooptim extension prefix.
      '!' + config.path.ASSETS_SRC + '/**/*.nooptim.*',
      // Exclude SVG webfonts.
      '!' + config.path.ASSETS_SRC + '/**/{fonts,fonts/**/*.svg}'
    ])
      .pipe(newer(config.path.ASSETS_DST))
      .pipe(imagemin([
        imagemin.gifsicle(),
        imagemin.jpegtran({progressive: true}),
        imagemin.optipng(),
        imagemin.svgo({
          plugins: [
            {removeViewBox: false}
          ]
        })
      ], {
        verbose: true
      }))
      .pipe(gulp.dest(config.path.ASSETS_DST)));

    tasks.push(gulp.src([
      config.path.ASSETS_SRC + '/**/*.nooptim.@(gif|jpg|png|svg|json)'
    ])
      .pipe(rename(function (path) {
        path.basename = path.basename.replace('.nooptim', '');
      }))
      .pipe(newer(config.path.ASSETS_DST))
      .pipe(gulp.dest(config.path.ASSETS_DST)));

    tasks.push(gulp.src([
      config.path.ASSETS_SRC + '/**/*.@(otf|ttf|woff2)'
    ])
      .pipe(newer(config.path.ASSETS_DST))
      .pipe(gulp.dest(config.path.ASSETS_DST)));

    return merge(tasks);
  };
};
