/* global module, require */

const gulp = require('gulp');
const fs = require('fs');
const crypto = require('crypto');
const newer = require('gulp-newer');
const iconfont = require('gulp-iconfont');

module.exports = function (config) {
  return function buildIcons() {
    let formats = ['eot', 'ttf', 'woff'];

    return gulp.src([config.path.ASSETS_SRC + '/themes/frontend/icons/*.svg'])
    //.pipe(newer(config.path.ASSETS_DST + '/themes/frontend/fonts/icons.woff'))
      .pipe(iconfont({
        formats: formats,
        fontName: 'icons',
        fontHeight: 1024,
        descent: 64,
        normalize: true,
        prependUnicode: false,
        timestamp: 1
      }))
      .on('glyphs', function (glyphs, options) {
        let stream = fs.createWriteStream(config.path.ASSETS_SRC + '/themes/frontend/css/config/_icons.scss', {flags: 'w'});
        stream.once('open', function (fd) {
          stream.write('// DON\'T MANUALLY EDIT THIS FILE; run `gulp build-icons` instead.\n');
          stream.write('$icons: (\n');
          glyphs.forEach(function (glyph) {
            stream.write('  ' + glyph.name + ': "\\' + glyph.unicode[0].charCodeAt(0).toString(16).toLowerCase() + '",\n');
          });
          stream.write(');\n');
          stream.end();
        });
      })
      .pipe(gulp.dest(config.path.ASSETS_DST + '/themes/frontend/fonts'))
      .on('finish', function () {
        let hash = crypto.createHash('md5');

        formats.forEach(function (ext) {
          return hash.update(fs.readFileSync(config.path.ASSETS_DST + '/themes/frontend/fonts/icons.' + ext));
        }, 'a');

        hash = hash.digest('hex').slice(0, 10);

        let stream = fs.createWriteStream(config.path.ASSETS_SRC + '/themes/frontend/css/config/_icons-cachebust.scss', {flags: 'w'});
        stream.once('open', function (fd) {
          stream.write('// DON\'T MANUALLY EDIT THIS FILE; run `gulp build-icons` instead.\n');
          stream.write('$icons-cachebust: "' + hash + '";\n');
          stream.end();
        });
      });
  };
};
