/* global module, require */

const gulp = require('gulp');
const newer = require('gulp-newer');

module.exports = function (config) {
  return function buildFonts() {
    return gulp.src(config.path.ASSETS_SRC + '/**/fonts/**/*.@(eot|otf|svg|ttf|woff|woff2)')
      .pipe(newer(config.path.ASSETS_DST))
      .pipe(gulp.dest(config.path.ASSETS_DST));
  };
};
