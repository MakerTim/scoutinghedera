#!/usr/bin/env bash

list-sites() {
  find app/sites/ -mindepth 1 -maxdepth 1 -type d -exec basename "{}" \; | sort
}

module-enabled() {
  local site=$1
  local module=$2

  vendor/bin/drush -l "$site" pm-list --pipe --type=module --status=enabled --field=name | grep -xq "$module"
}

update-site() {
  local site=$1

  echo "Rebuilding Drupal container..."
  vendor/bin/drush -l "$site" php:eval "\Drupal::service('kernel')->rebuildContainer(); echo 'Done' . PHP_EOL;"

  echo "Clearing critical Drupal caches..."
  vendor/bin/drush -l "$site" cache:clear bin bootstrap,data,default,discovery -y

  echo "Clearing twig template cache..."
  vendor/bin/drush -l "$site" php:eval "\Drupal::service('twig')->invalidate(); echo 'Done' . PHP_EOL;"

  echo "Applying available module updates..."
  vendor/bin/drush -l "$site" updatedb -y

  echo "Importing configuration changes..."
  vendor/bin/drush -l "$site" config:import -y

  echo "Importing translation changes..."
  if module-enabled "$site" "locale"; then
    vendor/bin/drush -l "$site" locale:check -y
    vendor/bin/drush -l "$site" locale:update -y
    vendor/bin/drush -l "$site" config:import -y

    for language in $(list-languages); do
      vendor/bin/drush -l "$site" locale:import "$language" "translations/backend.$language.po" --override=all
      vendor/bin/drush -l "$site" locale:import "$language" "translations/custom.$language.po" --override=all
      vendor/bin/drush -l "$site" locale:import "$language" "translations/frontend.$language.po" --override=all
      vendor/bin/drush -l "$site" locale:import "$language" "translations/mailing.$language.po" --override=all
    done
  else
    echo "Skipped (locale module not enabled)"
  fi

  echo "Clearing all Drupal caches..."
  vendor/bin/drush -l "$site" cache:rebuild -y

  echo "Running deploy hooks"
  vendor/bin/drush -l "$site" deploy:hook -y

  echo "Updating crontab..."
  bash scripts/sh/check-crontab.sh

  echo "Block root user..."
  vendor/bin/drush -l "$site" user:block --uid=1
}

main() {
  echo "Restoring write protection for configuration files..."
  chmod 400 .env .env.*
  chmod -R u-w,go-rwX app/sites

  echo "Installing PHP libraries using Composer..."
  COMPOSER_DISCARD_CHANGES=true composer install --no-interaction --no-dev --prefer-dist --optimize-autoloader

  bash scripts/sh/check-symlinks.sh
  bash scripts/sh/publish-assets.sh

  echo "Synchronizing webservers..."
  local deploy_path; deploy_path="$(realpath "$(pwd)")/"
  if command -v rsync.sh >/dev/null 2>&1 && echo "$deploy_path" | grep -q "^/data/local/"; then
    echo "With basepath $deploy_path"
    rsync.sh sync "$deploy_path" --stats
  else
    echo "Webserver sync tool not found or not deploying to /data/local/"
    echo "Skipped"
  fi

  echo "Updating sites..."
  for site in $(list-sites); do
    echo "Updating site '${site}'..."
    update-site "$site"
  done
}

(
  set -euo pipefail
  main
)
