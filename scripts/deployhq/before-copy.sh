#!/usr/bin/env bash

main() {
  echo "Removing write protection for configuration files..."
  chmod 600 .env .env.*
  chmod -R u+rwX,go-rwX app/sites
}

(
  set -euo pipefail
  main
)
