#!/usr/bin/env bash

list-sites() {
  find app/sites/ -mindepth 1 -maxdepth 1 -type d -exec basename "{}" \; | sort
}

main() {
  for site in $(list-sites); do
    nice vendor/bin/drush -l "$site" --quiet cron &
  done
}

(
  set -euo pipefail
  main
)
