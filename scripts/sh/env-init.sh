#!/usr/bin/env bash

check_env_exists() {
  echo -n "Checking if .env file exists... "
  if [ ! -e .env ]; then
    cp .env.example .env
    echo "Created"
  else
    echo "Ok"
  fi
}

print_help() {
  local bold="\e[1m"
  local reset="\e[0m\e[39m"
  local green="\e[32m"
  local yellow="\e[93m"
  echo -e "$(
    cat <<HELP
${bold}Usage:${reset} env-init ${green}[...arguments]${reset}

${yellow}Available arguments:${reset}
  ${green}-h=host | --host=host${reset}      fills the ${yellow}DB_HOST${reset} field inside the ${yellow}.env${reset} file
  ${green}-u=user | --user=user${reset}      fills the ${yellow}DB_USER${reset} field inside the ${yellow}.env${reset} file
  ${green}-p=pass | --password=pass${reset}  fills the ${yellow}DB_PASS${reset} field inside the ${yellow}.env${reset} file
  ${green}-n=name | --name=name${reset}      fills the ${yellow}DB_NAME${reset} field inside the ${yellow}.env${reset} file
  ${green}-s=secr | --secret=secr${reset}    fills the ${yellow}APP_SECRET${reset} field inside the ${yellow}.env${reset} file
  ${green}-h | -? | --help${reset}           prints this help information page
HELP
  )"
  echo
}

parse_arguments() {
  for i in "$@"; do
  case $i in
    -h=*|--host=*)
      DB_HOST="${i#*=}"
      shift
      ;;
    -u=*|--user=*)
      DB_USER="${i#*=}"
      shift
      ;;
    -p=*|--password=*)
      DB_PASS="${i#*=}"
      shift
      ;;
    -n=*|--name=*)
      DB_NAME="${i#*=}"
      shift
      ;;
    -s=*|--secret=*)
      APP_SECRET="${i#*=}"
      shift
      ;;
    -h|-?|--help)
      print_help
      exit 0
      ;;
    *)
      local reset="\e[0m\e[39m"
      local red="\e[91m"
      local yellow="\e[93m"
      echo
      echo -e "${red}Unknown argument ${yellow}$i${reset}"
      echo
      print_help
      exit 2
      ;;
  esac
done
}

fill_env_variables() {
  echo "Checking required env variables... "
  local env_variables="DB_HOST DB_USER DB_PASS DB_NAME APP_SECRET"
  local env_var_value;
  for env_variable in $env_variables; do
    if grep --quiet "^${env_variable}=$" .env; then
      if [ -n "${!env_variable-}" ]; then
        env_var_value="${!env_variable}"
        echo "${env_variable}: ${!env_variable}"
      else
        if [ "$env_variable" = "APP_SECRET" ]; then
          env_var_value=$(openssl rand 55 | base64 | tr '+/=' '-_')
          echo "APP_SECRET: ${env_var_value}"
        else
          read -p "${env_variable}: " env_var_value
        fi
      fi
      sed -i.bak "/^${env_variable}=$/ s/$/${env_var_value}/" .env && rm .env.bak
    fi
  done
}

enable_developer_settings() {
  if [ ! -e app/sites/default/settings.local.php ]; then
    cp app/sites/example.settings.local.php app/sites/default/settings.local.php
  fi
}

main() {
  parse_arguments "$@"
  check_env_exists
  fill_env_variables
  enable_developer_settings
}

(
  set -euo pipefail
  main "$@"
)
