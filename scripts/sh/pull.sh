#!/usr/bin/env bash

echo-comment() {
  echo -e "\e[33m$*\e[0m";
}

echo-info() {
  echo -e "\e[32m$*\e[0m";
}

confirmed-continue() {
  echo -e -n "$(echo-info "$1" "(yes/no)") [$(echo-comment "no")]: "
  read -r
  case $(echo "$REPLY" | tr '[:upper:]' '[:lower:]') in
    y|yes) return 0 ;;
    *)     return 1 ;;
  esac
}

check-unexported-configuration() {
  if drush status bootstrap 2>/dev/null | grep -q Successful; then
    local config_changes; config_changes=$(drush cst --format=list 2>/dev/null | wc -l)

    if [ "$config_changes" -gt 0 ]; then
      echo -e "You have ${config_changes} unexported configuration files:"
      vendor/bin/drush cst
      if ! confirmed-continue "Ignore configuration and continue?"; then
        return 1
      fi
    else
      echo -e "You have no unexported configuration files."
    fi
  fi
}

use-stage-file-proxy() {
  source .env

  if [ "${STAGE_FILE_PROXY:-1}" == 0 ]; then
    return 1
  fi

  return 0
}

main() {
  local source=$1

  check-unexported-configuration

  vendor/bin/drush sql:create || true

  local extra_dump_args=""
  if vendor/bin/drush @${source} ssh -- mysqldump --help | grep -q 'column-statistics'; then
    extra_dump_args="$extra_dump_args --column-statistics=0"
  fi
  vendor/bin/drush sql:sync "@${source}" @self --extra-dump="$extra_dump_args" || true

  vendor/bin/drush sql:sanitize || true

  if use-stage-file-proxy; then
    vendor/bin/drush en stage_file_proxy
  else
    vendor/bin/drush rsync "@${source}":%files @self:%files --exclude-paths=css:js:styles:google_tag || true
  fi

  vendor/bin/drush rsync "@${source}":%private @self:%private || true
  vendor/bin/drush cache:rebuild
}

(
  set -euo pipefail
  main "$@"
)
