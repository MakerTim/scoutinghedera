#!/usr/bin/env bash

check_symlink() {
  local symlink=$1
  local target=$2

  if [ "$symlink" -ef "$(dirname "$symlink")/$target" ]; then
    echo "OK ($symlink -> $target)"
  elif [ -e  "$symlink" ]; then
    echo "FAIL ($symlink -> $target)"
    return 1;
  else
    ln -s "$target" "$symlink"
    echo "FIXED ($symlink -> $target)"
  fi
}

main() {
  echo "Checking project symlinks..."

  check_symlink "app/assets" "../public_html/assets"
  check_symlink "app/libraries" "../public_html/libraries"
  check_symlink "app/uploads" "../storage/uploads-public"

  check_symlink "public_html/uploads" "../storage/uploads-public"
}

(
  set -euo pipefail
  main
)
