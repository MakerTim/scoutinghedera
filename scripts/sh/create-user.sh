#!/usr/bin/env bash

main() {
  local alias=$1
  local mail=$2
  local role=$3

  if ! vendor/bin/drush "@${alias}" uinf ${mail} --quiet >/dev/null 2>&1; then
      vendor/bin/drush "@${alias}" ucrt ${mail} --mail=${mail}
  else
    echo "${mail} already exists"
  fi

  vendor/bin/drush "@${alias}" urol ${role} ${mail} || true
  vendor/bin/drush "@${alias}" uli --name=${mail}
}

(
  set -euo pipefail
  main "$@"
)
