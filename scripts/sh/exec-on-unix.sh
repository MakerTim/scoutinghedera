#!/usr/bin/env bash

main() {
  if [ -x "$(command -v wsl.exe)" ]; then
    wsl.exe "$@"
  else
    "$@"
  fi
}

(
  set -euo pipefail
  main "$@"
)
