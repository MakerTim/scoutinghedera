Feature: Basic text lanes
  In order to be impressed
  As a visitor
  There needs to be lane-based page content type

  @api
  Scenario: Viewing a page with a text lane
    Given I am viewing a "page":
      | title | My page |
    And the page__body contains a "text" paragraph:
      | body:value  | <p>PLACEHOLDER BODY.</p> |
      | body:format | basic_html               |
    Then I should see the heading "My page"
    And I should see the text "PLACEHOLDER BODY." in the "content" region
