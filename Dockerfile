ARG IMAGE_BASE_APP=swisleiden/base-php-8.1-fpm:1
ARG IMAGE_BASE_WEB=swisleiden/base-nginx:1
ARG IMAGE_BUILDER_PHP=swisleiden/fluo-dev-php:2
ARG IMAGE_BUILDER_NODE=swisleiden/fluo-dev-node:1

# Base images
# ===========

# Install any additional (binary) dependencies for the app server.
FROM $IMAGE_BASE_APP AS app-base

# Configure the app server for development environments.
FROM app-base AS app-dev

# Install any additional (binary) dependencies for the web server.
FROM $IMAGE_BASE_WEB AS web-base

COPY resources/docker/nginx/nginx-conf.d /etc/nginx/templates/

# Configure the web server for development environments.
FROM web-base AS web-dev

# Install any additional (binary) dependencies for the builder.
FROM $IMAGE_BUILDER_PHP AS builder-base

# Install any additional (binary) dependencies for the node builder.
FROM $IMAGE_BUILDER_NODE AS builder-node-base

# Builder images
# ==============

# Install PHP dependencies.
FROM builder-base AS builder-php

COPY composer.* /srv/
COPY scripts/composer /srv/scripts/composer

RUN composer install \
    --no-ansi \
    --no-interaction \
    --no-dev \
    --no-autoloader

# Install node dependencies and build assets.
FROM builder-node-base AS builder-node

COPY \
  # Sources:
  package.json \
  package-lock.json \
  # Destination:
  /srv/

RUN npm ci

COPY scripts/gulp /srv/scripts/gulp
COPY scripts/sh   /srv/scripts/sh

COPY \
  # Sources:
  .browserslistrc \
  .eslintignore \
  .eslintrc \
  .stylelintrc \
  gulpfile.js \
  gulpconfig.json \
  # Destination:
  /srv/

COPY --from=builder-php /srv/app /srv/app
# TODO publish/merge assets during gulp build.
RUN bash scripts/sh/publish-assets.sh

COPY resources/assets /srv/resources/assets

RUN gulp build

# Combine results of the builders.
FROM builder-php AS builder

# Note that the order is based upon the size of the
# directories and how often things typically change.

#COPY --from=builder-php  /srv/app                   /srv/app
#COPY --from=builder-php  /srv/vendor                /srv/vendor
#COPY --from=builder-php  /srv/storage/translations  /srv/storage/translations
COPY --from=builder-node /srv/public_html           /srv/public_html

COPY composer.*   /srv/
COPY drush        /srv/drush
COPY public_html  /srv.src/public_html
COPY app          /srv.src/app

RUN echo n | cp -ipr /srv.src/* /srv/ \
  && rm -r /srv.src \
  && composer dump-autoload --optimize

# TODO move/refactor
RUN true \
  && ln -s ../public_html/assets     /srv/app/assets \
  && ln -s ../public_html/libraries  /srv/app/libraries \
  && ln -s ../storage/uploads-public /srv/app/uploads \
  && ln -s ../storage/uploads-public /srv/public_html/uploads

# TODO move/refactor
COPY scripts/cron/crontab.tpl /srv/crontab
RUN sed -i 's#${PROJECT_ROOT}#/srv/#; s#bash scripts/sh/run-cron.sh#drush cron#' /srv/crontab

# Production images
# =================

# Finalize the app server image.
FROM app-base AS app

COPY --from=builder /srv /srv

VOLUME /srv/storage/uploads-private
VOLUME /srv/storage/uploads-public

# This image supports to be run by an arbitrary uid.
# The primary gid should be root (0).
# The default uid is nonroot (10001) but can be any uid.
# To support this, directories and files that are written to by processes in
# the image must be owned by the root group and be read/writable by that group.
# Files to be executed must also have group execute permissions.
USER nonroot:root

# https://github.com/opencontainers/image-spec/blob/main/annotations.md
ARG NAME
ARG VERSION
ARG CREATED
ARG SOURCE
ARG REVISION=HEAD
LABEL \
  org.opencontainers.image.title="$NAME" \
  org.opencontainers.image.description="App server for $NAME using PHP FPM." \
  org.opencontainers.image.version="$VERSION" \
  org.opencontainers.image.created="$CREATED" \
  org.opencontainers.image.source="$SOURCE" \
  org.opencontainers.image.revision="$REVISION"

# Finalize the web server image.
FROM web-base AS web

COPY --from=builder /srv/public_html /srv/public_html

VOLUME /srv/storage/uploads-public

# This image supports to be run by an arbitrary uid.
USER nonroot:root

ARG NAME
ARG VERSION
ARG CREATED
ARG SOURCE
ARG REVISION=HEAD
LABEL \
  org.opencontainers.image.title="$NAME/web" \
  org.opencontainers.image.description="Web server for $NAME using NGINX." \
  org.opencontainers.image.version="$VERSION" \
  org.opencontainers.image.created="$CREATED" \
  org.opencontainers.image.source="$SOURCE" \
  org.opencontainers.image.revision="$REVISION"
