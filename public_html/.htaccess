
# Don't show directory listings for URLs which map to a directory.
Options -Indexes

# Set the default handler.
DirectoryIndex index.php

<IfModule mod_mime.c>
  # Add correct content type header and encoding for SVGZ.
  AddType image/svg+xml svg svgz
  AddEncoding gzip svgz

  # Add correct content type header for woff and woff2 fonts.
  AddType application/x-font-woff .woff
  AddType application/font-woff2 .woff2
</IfModule>

# PHP settings.
<IfModule mod_php7.c>
  php_value assert.active                   0
  php_flag session.auto_start               off
  php_value mbstring.http_input             pass
  php_value mbstring.http_output            pass
  php_flag mbstring.encoding_translation    off

  # Enable gzipping out HTML output
  php_flag zlib.output_compression          1
</IfModule>

<IfModule mod_expires.c>
  ExpiresActive On

  # By default regard files to be static and immutable.
  ExpiresDefault "access plus 1 year"

  <FilesMatch \.php$>
    # Do not allow PHP scripts to be cached unless they explicitly send cache
    # headers themselves. Otherwise all scripts would have to overwrite the
    # headers set by mod_expires if they want another caching behavior. This may
    # fail if an error occurs early in the bootstrap process, and it may cause
    # problems if a non-Drupal PHP file is installed in a subdirectory.
    ExpiresActive Off
  </FilesMatch>
</IfModule>

<ifmodule mod_deflate.c>
  AddOutputFilterByType DEFLATE application/xml
  AddOutputFilterByType DEFLATE application/xhtml+xml
  AddOutputFilterByType DEFLATE application/rss+xml
  AddOutputFilterByType DEFLATE application/javascript
  AddOutputFilterByType DEFLATE application/json
  AddOutputFilterByType DEFLATE application/x-javascript
  AddOutputFilterByType DEFLATE image/svg+xml
  AddOutputFilterByType DEFLATE text/plain
  AddOutputFilterByType DEFLATE text/html
  AddOutputFilterByType DEFLATE text/xml
  AddOutputFilterByType DEFLATE text/css
</ifmodule>

# Various rewrite rules.
<IfModule mod_rewrite.c>
  RewriteEngine on

  # Ensure the environment variable HTTPS (that is ENV:HTTPS, not the
  # special HTTPS variable) exists.
  RewriteCond %{ENV:HTTPS} ''
  RewriteCond %{HTTPS} on
  RewriteRule ^ - [E=HTTPS:on]

  # Set "protossl" to "s" if we were accessed via https://.  This is used later
  # if you enable "www." stripping or enforcement, in order to ensure that
  # you don't bounce between http and https.
  RewriteRule ^ - [E=protossl]
  RewriteCond %{ENV:HTTPS} on
  RewriteRule ^ - [E=protossl:s]

  # Make sure Authorization HTTP header is available to PHP
  # even when running as CGI or FastCGI.
  RewriteRule ^ - [E=HTTP_AUTHORIZATION:%{HTTP:Authorization}]

  # Block access to "hidden" directories whose names begin with a period. This
  # includes directories used by version control systems such as Subversion or
  # Git to store control files. Files whose names begin with a period, as well
  # as the control files used by CVS, are protected by the FilesMatch directive
  # above.
  #
  # NOTE: This only works when mod_rewrite is loaded. Without mod_rewrite, it is
  # not possible to block access to entire directories from .htaccess because
  # <DirectoryMatch> is not allowed here.
  #
  # If you do not have mod_rewrite installed, you should remove these
  # directories from your webroot or otherwise protect them from being
  # downloaded.
  RewriteRule "(^|/)\.(?!well-known)" - [F]

  # Always redirect to https.
  RewriteCond %{HTTP_HOST} !\.swisdev\.nl$ [NC]
  RewriteCond %{ENV:APP_URL} !^http:// [NC]
  RewriteCond %{ENV:HTTPS} !=on
  RewriteRule .* https://%{HTTP_HOST}%{REQUEST_URI} [R=301,L]

  # Always redirect to domain with www prefix.
  RewriteCond %{HTTP_HOST} ^hedera\.nl$ [NC]
  RewriteRule ^ http%{ENV:protossl}://www.%{HTTP_HOST}%{REQUEST_URI} [R=301,L]

  # Remove index.php from urls.
  RewriteCond %{THE_REQUEST} ^[A-Z]{3,9}\ /index\.php\ HTTP/
  RewriteRule ^ http%{ENV:protossl}://%{HTTP_HOST}/ [R=301,L]

  RewriteRule ^index\.php/(.*) http%{ENV:protossl}://%{HTTP_HOST}/$1 [R=301,L]

  # Serve favicons from the /favicons/ directory.
  RewriteRule ^favicon\.ico$ /favicons/favicon.ico [L]
  RewriteRule ^((apple-touch-icon|favicon|mstile)(\-[0-9x]+|precomposed)?\.png)$ /favicons/$1 [L]
  RewriteRule ^browserconfig\.xml$ /favicons/browserconfig.xml [L]

  # Serve site logo/icon from the /assets/ directory.
  RewriteRule ^((icon|logo)([-.][^.]+)?\.(png|svg))$ /assets/$1 [L]

  # Serve Drupal assets from the /assets/ directory.
  RewriteCond %{REQUEST_URI} ^/(core|modules|profiles|themes)/
  RewriteCond %{REQUEST_URI} \.(css|js|map|jpg|png|gif|svg|eot|ttf|woff|woff2|otf)$
  RewriteRule ^(.*)$ /assets/$1 [L]

  # Deny access to Drupal core's web installers.
  RewriteRule ^core/(authorize|install|update)\.php$ - [F]

  # Pass all requests not referring directly to files in the filesystem to
  # index.php.
  RewriteCond %{REQUEST_FILENAME} !-f
  RewriteRule ^ index.php [L]

  # Rules to correctly serve gzip compressed files.
  # Requires both mod_rewrite and mod_headers to be enabled.
  <IfModule mod_headers.c>
    # Serve gzip compressed files if they exist and the client accepts gzip.
    RewriteCond %{HTTP:Accept-encoding} gzip
    RewriteCond %{REQUEST_FILENAME}.gz -s
    RewriteRule ^(.*)\.(css|js|svg)$ $1.$2.gz [QSA]

    # Serve correct content types, and prevent mod_deflate double gzip.
    RewriteRule \.css\.gz$ - [T=text/css,E=no-gzip:1]
    RewriteRule \.js\.gz$ - [T=text/javascript,E=no-gzip:1]
    RewriteRule \.svg\.gz$ - [T=image/svg+xml,E=no-gzip:1]

    <FilesMatch "\.(css|js|svg)\.gz$">
      # Serve correct encoding type.
      Header set Content-Encoding gzip
      # Force proxies to cache gzipped & non-gzipped css/js files separately.
      Header append Vary Accept-Encoding
    </FilesMatch>
  </IfModule>
</IfModule>

# Security headers.
# Use "always" to ensure the headers are set even on error responses, and
# persisted across internal redirects.
<IfModule mod_headers.c>
  Header always set Strict-Transport-Security "max-age=31536000; includeSubDomains" env=HTTPS
  Header always set Referrer-Policy "no-referrer-when-downgrade"
  Header always set X-Xss-Protection "1; mode=block"
  # Disable content sniffing, since it's an attack vector.
  Header always set X-Content-Type-Options nosniff
  # Disable Proxy header, since it's an attack vector.
  RequestHeader unset Proxy
</IfModule>
