version: "3.4"

# Compose file for development, test or ci environments.

x-app-user: &app-user
  user: $COMPOSE_UID:$COMPOSE_GID
  group_add:
    - $COMPOSE_GID
    - $COMPOSE_UID

x-build-defaults: &build-defaults
  context: .
  cache_from:
    - ${IMAGE_BASE}
    - ${IMAGE_BASE}.web
    - ${IMAGE_BASE}.app-dev
    - ${IMAGE_BASE}.web-dev
    - ${IMAGE_BASE}.builder
    - ${IMAGE_BASE}.builder-node
    - ${IMAGE_BASE}.builder-php
    - ${IMAGE_HEAD}
    - ${IMAGE_HEAD}.web
    - ${IMAGE_HEAD}.app-dev
    - ${IMAGE_HEAD}.web-dev
    - ${IMAGE_HEAD}.builder
    - ${IMAGE_HEAD}.builder-node
    - ${IMAGE_HEAD}.builder-php

x-build-prod-defaults: &build-prod-defaults
  <<: *build-defaults
  args:
  - NAME=${IMAGE_NAME:?missing IMAGE_NAME}
  - VERSION=${IMAGE_VERSION:?missing IMAGE_VERSION}
  - CREATED=${IMAGE_CREATED:?missing IMAGE_CREATED}
  - SOURCE=${IMAGE_SOURCE:?missing IMAGE_SOURCE}
  - REVISION=${IMAGE_REVISION:?missing IMAGE_REVISION}

x-service-builder: &builder
  profiles: [ "builders", "push-targets" ]
  entrypoint: "false"
  restart: "no"

x-service-tool: &x-tool
  profiles: [ "tools" ]
  <<: *app-user
  env_file: .env
  volumes:
    - ./:/srv/
  entrypoint: "false"
  restart: "no"

x-service-tool-php: &tool-php
  <<: *x-tool
  image: ${IMAGE_HEAD}.builder-base
  build:
    <<: *build-defaults
    target: builder-base

x-service-tools-node: &tool-node
  <<: *x-tool
  image: ${IMAGE_HEAD}.builder-node-base
  build:
    <<: *build-defaults
    target: builder-node-base

services:
  app-prod:
    profiles: [ "prod", "push-targets" ]
    image: ${IMAGE_HEAD}
    build:
      <<: *build-prod-defaults
      target: app
    <<: *app-user
    env_file: .env
    environment:
      APP_ENV: prod
    read_only: true
    volumes:
      - ./storage/uploads-private:/srv/storage/uploads-private
      - ./storage/uploads-public:/srv/storage/uploads-public
    tmpfs:
      - /srv/storage/compiled:uid=$COMPOSE_UID,gid=$COMPOSE_GID
      - /tmp:uid=$COMPOSE_UID,gid=$COMPOSE_GID
    restart: unless-stopped
    expose:
      - 9000
    networks:
      backend:

  app-dev: &app-dev
    profiles: [ "dev", "push-targets" ]
    image: ${IMAGE_HEAD}.app-dev
    build:
      <<: *build-defaults
      target: app-dev
    <<: *app-user
    env_file: .env
    volumes:
      - ./:/srv/
    tmpfs:
      - /tmp:uid=$COMPOSE_UID,gid=$COMPOSE_GID
    restart: unless-stopped
    expose:
      - 9000
    networks:
      backend:

  web-prod:
    profiles: [ "prod", "push-targets" ]
    image: ${IMAGE_HEAD}.web
    build:
      <<: *build-prod-defaults
      target: web
    <<: *app-user
    environment:
      NGINX_HOST: $APP_DOMAIN
      NGINX_BACKEND_APP: app-prod
    read_only: true
    volumes:
      - ./storage/uploads-public:/srv/storage/uploads-public
    tmpfs:
      - /etc/nginx/conf.d
      - /tmp:uid=$COMPOSE_UID,gid=$COMPOSE_GID
    expose:
      - 8080
    networks:
      backend:
    depends_on:
      - app-prod

  web-dev:
    profiles: [ "dev", "push-targets" ]
    image: ${IMAGE_HEAD}.web-dev
    build:
      <<: *build-defaults
      target: web-dev
    <<: *app-user
    environment:
      NGINX_HOST: $APP_DOMAIN
      NGINX_BACKEND_APP: app-dev
    volumes:
      - ./public_html:/srv/public_html
    tmpfs:
      - /tmp:uid=$COMPOSE_UID,gid=$COMPOSE_GID
    expose:
      - 8080
    networks:
      backend:
    depends_on:
      - app-dev

  builder-php:
    <<: *builder
    image: ${IMAGE_HEAD}.builder-php
    build:
      <<: *build-defaults
      target: builder-php

  builder-node:
    <<: *builder
    image: ${IMAGE_HEAD}.builder-node
    build:
      <<: *build-defaults
      target: builder-node

  builder:
    <<: *builder
    image: ${IMAGE_HEAD}.builder
    build:
      <<: *build-defaults
      target: builder

  ingress:
    profiles: [ "ingress" ]
    image: "hitch:latest"
    command: --backend=[web-$APP_ENV]:8080 --alpn-protos=http/1.1 --write-proxy-v2=off
    expose:
      - 443
    networks:
      backend:
        aliases:
          - $APP_DOMAIN
      frontend:
        aliases:
          - $APP_DOMAIN
    ports:
      - "443:443"
    depends_on:
      - web-$APP_ENV

  db:
    profiles: [ "db" ]
    image: ${IMAGE_HEAD}.demo-db
    build:
      context: resources/docker/demo-db
    environment:
      - TZ
      - MYSQL_RANDOM_ROOT_PASSWORD=yes
      - MYSQL_USER=$DB_USER
      - MYSQL_PASSWORD=$DB_PASS
      - MYSQL_DATABASE=$DB_NAME
    restart: unless-stopped
    expose:
      - 3306
    networks:
      - backend

  redis:
    profiles: [ "redis" ]
    image: swisleiden/fluo-dev-redis
    command: redis-server --requirepass "$REDIS_PASSWORD"
    environment:
      - TZ
      - REDIS_PASSWORD
    restart: unless-stopped
    expose:
      - 6379
    networks:
      - backend

  composer:
    <<: *tool-php
    entrypoint: composer
    tmpfs:
      # TODO mount composer cache in bitbucket pipelines
      - /.composer

  mysqladmin: &mysqladmin
    <<: *tool-php
    entrypoint: mysqladmin

  phpcs:
    <<: *tool-php
    entrypoint: phpcs

  phpmd:
    <<: *tool-php
    entrypoint: phpmd
    command: [
      "app/modules/custom,app/themes/backend,app/themes/frontend",
      "text",
      "phpmd.xml.dist"
    ]

  phpstan:
    <<: *tool-php
    entrypoint: phpstan

  phpunit:
    <<: *tool-php
    entrypoint: phpunit
    environment:
      SIMPLETEST_DB: sqlite://localhost//dev/shm/simpletest.db

  gulp:
    <<: *tool-node
    entrypoint: gulp

  tools-app: &tool-app
    <<: *app-dev
    profiles: [ "tools" ]
    entrypoint: "false"

  behat:
    <<: *tool-app
    entrypoint: behat
    command: [
      "--profile=local"
    ]

  drush:
    <<: *tool-app
    entrypoint: drush

  task-db-wait:
    <<: *mysqladmin
    command: [
      "ping",
      "--host=$DB_HOST",
      "--user=$DB_USER",
      "--password=$DB_PASS",
      "--connect-timeout=10",
      "--wait=10"
    ]

networks:
  backend:
  frontend:
