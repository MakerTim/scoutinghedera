<?php

namespace Drupal\Tests\custom_base\Kernel;

use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;
use Drupal\node\Entity\Node;
use Drupal\node\Entity\NodeType;

class ExampleKernelTest extends EntityKernelTestBase {

  protected static $modules = ['node'];

  protected function setUp(): void {
    parent::setUp();

    NodeType::create([
      'type' => 'test_type',
      'name' => 'Test',
    ])->save();
    FieldStorageConfig::create([
      'entity_type' => 'node',
      'type' => 'string',
      'field_name' => 'test_field',
    ])->save();
    FieldConfig::create([
      'entity_type' => 'node',
      'bundle' => 'test_type',
      'field_name' => 'test_field',
    ])->save();
  }

  public function testKernelTests(): void {
    $node = Node::create([
      'type' => 'test_type',
      'title' => 'test title',
      'test_field' => 'test value',
    ]);

    $result = $node->save();

    $this->assertEquals($result, SAVED_NEW);
  }

}
