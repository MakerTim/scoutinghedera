<?php

namespace Drupal\Tests\custom_base\Unit;

use Drupal\Tests\UnitTestCase;

class ExampleUnitTest extends UnitTestCase {

  public function testUnitTests(): void {
    $stack = [];
    $this->assertSame(0, count($stack));

    array_push($stack, 'foo');
    $this->assertSame('foo', $stack[count($stack) - 1]);
    $this->assertSame(1, count($stack));
  }

}
