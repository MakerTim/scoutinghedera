<?php

namespace Drupal\custom_title_hidden\Plugin\Condition;

use Drupal\Core\Condition\ConditionPluginBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\node\NodeInterface;

/**
 * Provides a 'Title hidden' condition.
 *
 * @Condition(
 *   id = "custom_title_hidden",
 *   label = @Translation("Title set to be hidden"),
 *   context_definitions = {
 *     "node" = @ContextDefinition("entity:node", label = @Translation("Node"), required = FALSE)
 *   }
 * )
 */
final class TitleHiddenCondition extends ConditionPluginBase {

  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form['enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#default_value' => $this->configuration['enabled'],
    ];
    return parent::buildConfigurationForm($form, $form_state);
  }

  public function defaultConfiguration(): array {
    return [
      'enabled' => FALSE,
    ] + parent::defaultConfiguration();
  }

  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    if ($form_state->getValue('enabled')) {
      $this->configuration['enabled'] = $form_state->getValue('enabled');

      parent::submitConfigurationForm($form, $form_state);
    }
    else {
      $this->configuration = [];
    }
  }

  public function summary(): TranslatableMarkup {
    return $this->t('The page title is set to be hidden');
  }

  public function evaluate(): bool {
    $node = $this->getContextValue('node');

    if (!$node instanceof NodeInterface) {
      return FALSE;
    }

    return $node->hasField('title_hidden') && $node->get('title_hidden')->value;
  }

}
