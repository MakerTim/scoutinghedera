<?php

namespace Drupal\custom_logging;

use Bugsnag\Client;
use Monolog\Handler\HandlerInterface;
use Monolog\Handler\NoopHandler;

final class BugsnagHandlerFactory {

  const RELEASE_STAGES = [
    'dev' => 'development',
    'acc' => 'acceptance',
    'prod' => 'production',
  ];

  const DEFAULT_RELEASE_STAGE = 'development';

  /**
   * @param int|string|mixed $level
   */
  public static function create($level, string $app_root): HandlerInterface {
    $api_key = getenv('BUGSNAG_API_KEY');
    if (!$api_key) {
      return new NoopHandler();
    }

    $client = Client::make($api_key);
    $client->setProjectRoot(dirname($app_root));
    $client->setReleaseStage(self::RELEASE_STAGES[getenv('APP_ENV')] ?? self::DEFAULT_RELEASE_STAGE);

    return new BugsnagHandler($client, $level);
  }

}
