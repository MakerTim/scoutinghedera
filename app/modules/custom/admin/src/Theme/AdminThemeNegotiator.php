<?php

namespace Drupal\custom_admin\Theme;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Theme\ThemeNegotiatorInterface;

final class AdminThemeNegotiator implements ThemeNegotiatorInterface {

  private ConfigFactoryInterface $configFactory;

  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
  }

  public function applies(RouteMatchInterface $route_match): bool {
    $route = $route_match->getRouteObject();
    return ($route && $route->getOption('_custom_admin_theme') === TRUE);
  }

  public function determineActiveTheme(RouteMatchInterface $route_match): string {
    return $this->configFactory->get('system.theme')->get('admin');
  }

}
