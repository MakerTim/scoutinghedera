<?php

namespace Drupal\custom_admin\EventSubscriber;

use Drupal\Core\Routing\RouteSubscriberBase;
use Drupal\system\Controller\SystemController;
use Symfony\Component\Routing\RouteCollection;

class AlterAdminMenuBlockPageRoutesSubscriber extends RouteSubscriberBase {

  protected function alterRoutes(RouteCollection $collection): void {
    foreach ($collection as $route) {
      $controllers = [
        '\\' . SystemController::class . '::systemAdminMenuBlockPage',
        '\\' . SystemController::class . '::overview',
      ];

      if (in_array($route->getDefault('_controller'), $controllers)) {
        $route->setRequirement('_custom_admin_submenu_access', 'admin');
      }
    }
  }

}
