<?php

namespace Drupal\custom_admin\EventSubscriber;

use Drupal\Core\Routing\AdminContext;
use Drupal\Core\Routing\RedirectDestinationInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\KernelEvents;

final class RedirectToAdminLoginSubscriber implements EventSubscriberInterface {

  private AccountInterface $currentUser;

  private RedirectDestinationInterface $redirectDestination;

  private AdminContext $adminContext;

  public function __construct(AccountInterface $current_user, RedirectDestinationInterface $redirect_destination, AdminContext $admin_context) {
    $this->currentUser = $current_user;
    $this->redirectDestination = $redirect_destination;
    $this->adminContext = $admin_context;
  }

  public function onKernelException(ExceptionEvent $event): void {
    $exception = $event->getThrowable();
    if (!($exception instanceof AccessDeniedHttpException)) {
      return;
    }

    if (!$this->adminContext->isAdminRoute()) {
      return;
    }

    $options = [];
    $options['query'] = $this->redirectDestination->getAsArray();
    $options['absolute'] = TRUE;

    if ($this->currentUser->isAnonymous()) {
      $url = Url::fromRoute('custom_admin.login', [], $options)->toString();
      $response = new RedirectResponse($url, 307);
      $event->setResponse($response);
    }
  }

  public static function getSubscribedEvents(): array {
    $events[KernelEvents::EXCEPTION][] = ['onKernelException', 2];
    return $events;
  }

}
